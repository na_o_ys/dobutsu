package game

import (
	"fmt"
)

type Position struct {
	Row int
	Col int
}

// 持ち駒
var BlackHand = Position{-1, 0}
var WhiteHand = Position{-1, 1}

func GetHandPos(c Color) Position {
	switch c {
	case Black:
		return BlackHand
	default:
		return WhiteHand
	}
}

type Move struct {
	From  Position
	To    Position
	Piece Piece
}

func (m Move) String() string {
	if m.IsFromHand() {
		return fmt.Sprintf("%s Hand -> (%d, %d)", m.Piece, m.To.Row, m.To.Col)
	}
	return fmt.Sprintf("%s (%d, %d) -> (%d, %d)", m.Piece, m.From.Row, m.From.Col, m.To.Row, m.To.Col)
}

func (m Move) IsFromHand() bool {
	return m.From == BlackHand || m.From == WhiteHand
}

var emRow = map[Color]int{
	Black: 0,
	White: 3,
}

func (m Move) EntersEmRow() bool {
	return m.To.Row == emRow[m.Piece.Owner]
}

func (m Move) CanPromote() bool {
	return m.Piece.Kind == Hiyoko && !m.IsFromHand() && m.EntersEmRow()
}

func (m Move) GetColor() Color {
	return m.Piece.Owner
}

func (m Move) BecomesChecked(b *Board) bool {
	b1 := DoMove(*b, m, m.GetColor().Turn())
	return b1.IsChecked(m.GetColor())
}
