package game

type Color int

const (
	Black Color = 0
	White       = 1
	Empty       = 2
)

type Turn Color

const (
	BlackT = Turn(Black)
	WhiteT = Turn(White)
	EmptyT = Turn(Empty)
)

func (c Color) Turn() Turn {
	return Turn(int(c))
}

func (t Turn) Color() Color {
	return Color(t)
}

func (c Color) String() string {
	switch c {
	case Black:
		return "先手"
	case White:
		return "後手"
	default:
		return "分け"
	}
}
