package game

import (
	"strings"
)

type Piece struct {
	Kind  PieceKind
	Owner Color
}

type PieceKind int

const (
	Blank PieceKind = iota
	Lion
	Hiyoko
	Niwatori
	Kirin
	Zo
)

var KindString = map[PieceKind]string{
	Blank:    "-",
	Lion:     "L",
	Hiyoko:   "H",
	Niwatori: "N",
	Kirin:    "K",
	Zo:       "Z",
}

var StringKind = map[string]PieceKind{
	"-": Blank,
	"L": Lion,
	"H": Hiyoko,
	"N": Niwatori,
	"K": Kirin,
	"Z": Zo,
}

var KindMovables = map[PieceKind][3][3]int{
	// 前, 中, 後
	Lion:     {{1, 1, 1}, {1, 0, 1}, {1, 1, 1}},
	Hiyoko:   {{0, 0, 0}, {0, 0, 0}, {0, 1, 0}},
	Niwatori: {{0, 1, 0}, {1, 0, 1}, {1, 1, 1}},
	Kirin:    {{0, 1, 0}, {1, 0, 1}, {0, 1, 0}},
	Zo:       {{1, 0, 1}, {0, 0, 0}, {1, 0, 1}},
}

func (a Piece) Equal(b Piece) bool {
	return a.Kind == b.Kind && a.Owner == b.Owner
}

func (k Piece) CanMove(dr int, dc int) bool {
	if k.Owner == Black {
		dr *= -1
		dc *= -1
	}
	return KindMovables[k.Kind][dr+1][dc+1] == 1
}

func (p Piece) String() string {
	switch p.Owner {
	case Black:
		return KindString[p.Kind]
	default:
		return strings.ToLower(KindString[p.Kind])
	}
}

func FromStr(text string) Piece {
	if text == "-" {
		return Piece{Blank, Empty}
	}
	c := Black
	if text == strings.ToLower(text) {
		c = White
	}
	return Piece{StringKind[strings.ToUpper(text)], c}
}
