package game

type PlayerFactory interface {
	Create(c Color) Player
	Name() string
}

type Player interface {
	NextMove(b *Board) Move
}
