package game_test

import (
	"./../ai"
	"fmt"
	"testing"
)

func TestDoMove(t *testing.T) {
	board := game.CreateBoard()
	turn := game.BlackT
	board = board.DoMoveS(game.Position{3, 2}, game.Position{2, 2}, turn)
	actual := []game.Piece{
		board.GetStone(game.Position{3, 2}),
		board.GetStone(game.Position{2, 2}),
	}
	expected := []game.Piece{
		game.Piece{game.Blank, game.Empty},
		game.Piece{game.Kirin, game.Black},
	}
	if !isSame(expected, actual) {
		t.Errorf("expected: %v, actual: %v", expected, actual)
	}
}

func TestRetrieve(t *testing.T) {
	board := game.CreateBoard()
	turn := game.BlackT
	board = board.DoMoveS(game.Position{2, 1}, game.Position{1, 1}, turn)
	actual := []game.Piece{
		board.GetStone(game.Position{2, 1}),
		board.GetStone(game.Position{1, 1}),
	}
	expected := []game.Piece{
		game.Piece{game.Blank, game.Empty},
		game.Piece{game.Hiyoko, game.Black},
	}
	if !isSame(expected, actual) {
		t.Errorf("expected: %v, actual: %v", expected, actual)
	}
	if hs := board.Hand(game.Black, game.Hiyoko); hs == 0 {
		t.Errorf("hiyoko")
	}
}

func TestMoves(t *testing.T) {
	board := game.CreateBoard()
	turn := game.BlackT
	board = board.DoMoveS(
		game.Position{3, 2},
		game.Position{2, 2},
		turn,
	)
	turn ^= 1
	board = board.DoMoveS(
		game.Position{1, 1},
		game.Position{2, 1},
		turn,
	)
	turn ^= 1
	board = board.DoMoveS(
		game.Position{3, 0},
		game.Position{2, 1},
		turn,
	)
	actual := []game.Piece{
		board.GetStone(game.Position{3, 0}),
		board.GetStone(game.Position{3, 2}),
		board.GetStone(game.Position{2, 1}),
		board.GetStone(game.Position{2, 2}),
		board.GetStone(game.Position{1, 2}),
	}
	expected := []game.Piece{
		game.Piece{game.Blank, game.Empty},
		game.Piece{game.Blank, game.Empty},
		game.Piece{game.Zo, game.Black},
		game.Piece{game.Kirin, game.Black},
		game.Piece{game.Blank, game.Empty},
	}
	if !isSame(expected, actual) {
		t.Errorf("expected: %v, actual: %v", expected, actual)
	}
	if hs := board.Hand(game.Black, game.Hiyoko); hs == 0 {
		t.Errorf("先手ひよこ持ち")
	}
	if hs := board.Hand(game.White, game.Hiyoko); hs == 0 {
		t.Errorf("後手ひよこ持ち")
	}
}

func TestPromote(t *testing.T) {
	board := game.CreateBoard()
	turn := game.BlackT
	board = board.DoMoveSs(2, 1, 1, 1, turn)
	turn ^= 1
	board = board.DoMoveSs(0, 2, 1, 1, turn)
	turn ^= 1
	m := game.Move{game.BlackHand, game.Position{1, 2}, game.Piece{game.Hiyoko, game.Black}}
	board = board.DoMove(m, turn)
	turn ^= 1
	board = board.DoMoveSs(0, 0, 1, 0, turn)
	turn ^= 1
	board = board.DoMoveSs(1, 2, 0, 2, turn)
	turn ^= 1
	if !board.
		GetPiece(game.Position{0, 2}).
		Equal(game.Piece{game.Niwatori, game.Black}) {
		t.Errorf("成失敗")
	}
}

// func TestEnd(t *testing.T) {
// 	b := game.FromText(
// 		[]string{
// 			"k - -",
// 			"l H H",
// 			"- Z L",
// 			"- - -",
// 		},
// 		[]string{
// 			"K:1",
// 			"z:1",
// 		},
// 	)
// 	b.Show()
// 	b = b.DoMove(b.Movables(game.WhiteT, false)[0], game.WhiteT)
// 	b.Show()
// 	fmt.Println(b.Movables(game.WhiteT, false))
// 	t.Errorf("hoge")
// }

func isSame(a, b []game.Piece) bool {
	for i, v := range a {
		if !v.Equal(b[i]) {
			fmt.Println(v, b[i])
			return false
		}
	}
	return true
}
