package game

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

const (
	Rows = 4
	Cols = 3
)

type Board struct {
	Main  [Rows][Cols]Piece
	Hands [2][6]int // 持ち駒の枚数 駒種類数 = 6
}

// 持ち駒の枚数
func (board Board) Hand(c Color, k PieceKind) int {
	return board.Hands[int(c)][int(k)]
}

func (b *Board) IncHand(c Color, k PieceKind) {
	b.Hands[int(c)][int(k)]++
}

func (b *Board) DecHand(c Color, k PieceKind) {
	b.Hands[int(c)][int(k)]--
}

func (board Board) Show() {
	for _, cols := range board.Main {
		for _, col := range cols {
			fmt.Print(col, " ")
		}
		fmt.Println()
	}
	fmt.Println()
	cs := []Color{Black, White}
	flg2 := false
	for _, c := range cs {
		flg := false
		for i, n := range board.Hands[c] {
			if n > 0 {
				fmt.Printf("%s: %d ", Piece{PieceKind(i), c}, n)
				flg = true
			}
		}
		if flg {
			fmt.Println()
			flg2 = true
		}
	}
	if flg2 {
		fmt.Println()
	}
}

func (b *Board) AddHand(kind PieceKind, turn Turn) {
	b.Hands[turn][kind]++
}

func (b *Board) GetPiece(p Position) Piece {
	return b.Main[p.Row][p.Col]
}

func (b *Board) GetStone(p Position) Piece {
	return b.Main[p.Row][p.Col]
}

func (b *Board) CanMove(turn Turn, from Position, to Position) bool {
	if to.Row < 0 || to.Row >= Rows || to.Col < 0 || to.Col >= Cols {
		return false
	}
	piece := b.Main[from.Row][from.Col]
	if piece.Kind == Blank || piece.Owner != Color(turn) || b.GetPiece(to).Owner == Color(turn) {
		return false
	}
	return piece.CanMove(to.Row-from.Row, to.Col-from.Col)
}

func (b *Board) IsChecked(c Color) bool {
	for _, move := range b.Movables((c ^ 1).Turn(), true) {
		if b.GetPiece(move.To).Equal(Piece{Lion, c}) {
			return true
		}
	}
	return false
}

func (b *Board) Movables(turn Turn, all bool) []Move {
	movables := make([]Move, 0, 100)
	for rc := 0; rc < Rows*Cols; rc++ {
		from := Position{rc / Cols, rc % Cols}
		for rc_t := 0; rc_t < 9; rc_t++ {
			dr, dc := (rc_t/3)-1, (rc_t%3)-1
			to := Position{from.Row + dr, from.Col + dc}
			if b.CanMove(turn, from, to) {
				move := Move{from, to, b.GetPiece(from)}
				// 王手がかかる手は打てない
				if all || !move.BecomesChecked(b) {
					movables = append(movables, move)
				}
			}
		}
	}

	// 王手かかってたら手駒打てない
	if !all && b.IsChecked(turn.Color()) {
		return movables
	}

	// 空きマス
	blanks := make([]Position, 0, 20)
	for rc := 0; rc < Rows*Cols; rc++ {
		p := Position{rc / Cols, rc % Cols}
		if b.GetPiece(p).Kind == Blank {
			blanks = append(blanks, p)
		}
	}
	for i, n := range b.Hands[int(turn)] {
		if n > 0 {
			for _, to := range blanks {
				move := Move{
					GetHandPos(Color(turn)),
					to,
					Piece{PieceKind(i), Color(int(turn))},
				}
				movables = append(movables, move)
			}
		}
	}

	return movables
}

func (b *Board) DoMove(move Move, turn Turn) Board {
	return DoMove(*b, move, turn)
}

func (b *Board) DoMoveS(from, to Position, turn Turn) Board {
	return DoMove(*b, Move{from, to, b.GetPiece(from)}, turn)
}

func (b *Board) DoMoveSs(fr, fc, tr, tc int, turn Turn) Board {
	return b.DoMoveS(Position{fr, fc}, Position{tr, tc}, turn)
}

func DoMove(board Board, move Move, turn Turn) Board {
	next := board

	// 移動先の駒を持ち駒にする
	if piece := next.Main[move.To.Row][move.To.Col]; piece.Kind != Blank {
		if piece.Kind == Niwatori {
			piece.Kind = Hiyoko
		}
		next.AddHand(piece.Kind, turn)
	}

	// 成
	if move.CanPromote() {
		move.Piece.Kind = Niwatori
	}

	// 移動先を置き換える
	next.Main[move.To.Row][move.To.Col] = move.Piece

	if move.IsFromHand() {
		next.DecHand(Color(int(turn)), move.Piece.Kind)
	} else {
		next.Main[move.From.Row][move.From.Col] = Piece{Blank, Empty}
	}
	return next
}

func CreateBoard() Board {
	blank := Piece{Kind: Blank, Owner: Empty}
	return Board{
		Main: [4][3]Piece{
			[...]Piece{
				{Kirin, White},
				{Lion, White},
				{Zo, White},
			},
			[...]Piece{
				blank,
				{Hiyoko, White},
				blank,
			},
			[...]Piece{
				blank,
				{Hiyoko, Black},
				blank,
			},
			[...]Piece{
				{Zo, Black},
				{Lion, Black},
				{Kirin, Black},
			},
		},
	}
}

func (b *Board) IsGameEnd(t Turn) bool {
	// トライの判定
	rs := []int{0, 3}
	cs := []Color{Black, White}
	for i := 0; i < 2; i++ {
		for c := 0; c < 3; c++ {
			if b.Main[rs[i]][c].Equal(Piece{Lion, cs[i]}) {
				return true
			}
		}
	}

	// 相手に指し手が無いと勝ち
	return len(b.Movables(t^1, false)) == 0
}

func FromText(rows []string, hs []string) Board {
	main := [Rows][Cols]Piece{}
	for r, s := range rows {
		for c, p := range strings.Split(s, " ") {
			main[r][c] = FromStr(p)
		}
	}
	hands := [2][6]int{}
	for _, h := range hs {
		s := strings.Split(h, ":")
		p := FromStr(s[0])
		n, _ := strconv.Atoi(s[1])
		hands[p.Owner][p.Kind] = n
	}
	return Board{main, hands}
}

func (b *Board) Bytes(t Turn) []byte {
	buffer := bytes.NewBufferString(t.Color().String())
	for _, cols := range b.Main {
		for _, col := range cols {
			buffer.WriteString(col.String())
		}
	}
	cs := []Color{Black, White}
	for _, c := range cs {
		for i, n := range b.Hands[c] {
			if n > 0 {
				buffer.WriteString(fmt.Sprintf("%s%d", Piece{PieceKind(i), c}, n))
			}
		}
	}
	return buffer.Bytes()
}
