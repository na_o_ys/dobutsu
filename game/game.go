package game

import (
	"crypto/md5"
	"encoding/hex"
)

type Game struct {
	mp        map[string]int
	ps        []Player
	afterMove func(Turn, *Board, Move)
	onFinish  func(Color)
}

func NewGame(blackP, whiteP Player, afterMove func(Turn, *Board, Move), onFinish func(Color)) Game {
	return Game{
		make(map[string]int),
		[]Player{blackP, whiteP},
		afterMove,
		onFinish,
	}
}

func (g *Game) Run() {
	b := CreateBoard()
	t := BlackT
	for {
		p := g.ps[t]
		m := p.NextMove(&b)
		b = b.DoMove(m, t)
		g.afterMove(t, &b, m)
		if g.IsDraw(&b, t) {
			g.onFinish(Empty)
			return
		}
		if b.IsGameEnd(t) {
			g.onFinish(t.Color())
			return
		}
		t ^= 1
	}
}

func (g *Game) IsDraw(b *Board, t Turn) bool {
	hasher := md5.New()
	hasher.Write(b.Bytes(t))
	hash := hex.EncodeToString(hasher.Sum(nil))
	g.mp[hash]++
	return g.mp[hash] >= 3
}
