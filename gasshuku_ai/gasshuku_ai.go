package gasshuku_ai

import (
	g "../game"
	"fmt"
	"math"
)

type GasshukuAI struct {
	Color    g.Color
	MaxDepth int
}

func (p GasshukuAI) Turn() g.Turn {
	return p.Color.Turn()
}

var pieceScore = map[g.PieceKind]int{
	g.Lion:     0,
	g.Hiyoko:   200,
	g.Kirin:    400,
	g.Zo:       500,
	g.Niwatori: 800,
}

var colorCoef = map[g.Color]int{g.Black: 1, g.White: -1}

const ControlPoint = 10

const Inf = math.MaxInt32 - 10

func eval(b *g.Board) int {
	score := 0
	for rc := 0; rc < g.Rows*g.Cols; rc++ {
		p := b.GetPiece(g.Position{rc / g.Cols, rc % g.Cols})
		if p.Kind == g.Blank {
			continue
		}
		score += pieceScore[p.Kind] * colorCoef[p.Owner]
	}
	for i := 0; i < 2; i++ {
		for j, n := range b.Hands[i] {
			score += pieceScore[g.PieceKind(j)] * n * colorCoef[g.Color(i)]
		}
	}

	for rc := 0; rc < g.Rows*g.Cols; rc++ {
		from := g.Position{rc / g.Cols, rc % g.Cols}
		p := b.GetPiece(from)
		if p.Kind == g.Blank {
			continue
		}
		for rc_t := 0; rc_t < 9; rc_t++ {
			dr, dc := (rc_t/3)-1, (rc_t%3)-1
			to := g.Position{from.Row + dr, from.Col + dc}
			if b.CanMove(p.Owner.Turn(), from, to) {
				score += ControlPoint * colorCoef[p.Owner]
			}
		}
	}
	return score
}

// 評価値, 詰み手数, 手
func (p GasshukuAI) alphaBeta(b *g.Board, turn g.Turn, d, alpha, beta int) (int, int, []g.Move) {
	if b.IsGameEnd(turn ^ 1) {
		return -Inf, d, nil
	}
	if d > p.MaxDepth {
		return eval(b) * colorCoef[turn.Color()], Inf, nil
	}
	var mvs = make([]g.Move, 1)
	mr := Inf
	for _, move := range b.Movables(turn, false) {
		nb := g.DoMove(*b, move, turn)
		s, r, _ := p.alphaBeta(&nb, turn^1, d+1, -beta, -alpha)
		s *= -1
		if s > alpha || s == alpha && r < mr {
			alpha = s
			mr = r
			mvs[0] = move
		}
		if alpha >= beta {
			return alpha, mr, mvs
		}
	}
	return alpha, mr, mvs
}

func (p GasshukuAI) NextMove(b *g.Board) g.Move {
	s, r, m := p.alphaBeta(b, p.Color.Turn(), 0, -Inf, Inf)
	fmt.Printf("%s score: %d", p.Color, s)
	if r < Inf {
		fmt.Printf(" %d 手詰め", r)
	}
	fmt.Println()
	return m[0]
}
