package main

import (
	f "../ai_factories"
	g "../game"
	"fmt"
)

var factories = f.Factories()

func printPrompt(turn string) {
	fmt.Printf("%sプレーヤを選んでください:\n", turn)
	for i, fa := range factories {
		fmt.Printf("[%d] %s\n", i, fa.Name())
	}
	fmt.Print("Input: ")
}

func main() {
	printPrompt("先手")
	var b int
	fmt.Scan(&b)
	printPrompt("後手")
	var w int
	fmt.Scan(&w)
	g.CreateBoard().Show()
	game := g.NewGame(
		factories[b].Create(g.Black),
		factories[w].Create(g.White),
		func(t g.Turn, b *g.Board, m g.Move) {
			fmt.Println(t.Color())
			fmt.Println(m)
			b.Show()
		},
		func(c g.Color) {
			if c == g.Empty {
				fmt.Println("引き分け")
			} else {
				fmt.Println(c, "の勝利なり！")
			}
		},
	)
	game.Run()
}
