package gasshuku_ai

import (
	g "../game"
	"fmt"
	"math"
	"time"
)

type ConcAI struct {
	Color    g.Color
	ThinkSec int
}

type RecValue struct {
	score   int
	r       int
	moves   []g.Move
	success bool
}

func (p ConcAI) Turn() g.Turn {
	return p.Color.Turn()
}

var pieceScore = map[g.PieceKind]int{
	g.Lion:     0,
	g.Hiyoko:   200,
	g.Kirin:    400,
	g.Zo:       500,
	g.Niwatori: 800,
}

var colorCoef = map[g.Color]int{g.Black: 1, g.White: -1}

const ControlPoint = 10

const Inf = math.MaxInt32 - 10

func eval(b *g.Board) int {
	score := 0
	for rc := 0; rc < g.Rows*g.Cols; rc++ {
		p := b.GetPiece(g.Position{rc / g.Cols, rc % g.Cols})
		if p.Kind == g.Blank {
			continue
		}
		score += pieceScore[p.Kind] * colorCoef[p.Owner]
	}
	for i := 0; i < 2; i++ {
		for j, n := range b.Hands[i] {
			score += pieceScore[g.PieceKind(j)] * n * colorCoef[g.Color(i)]
		}
	}

	for rc := 0; rc < g.Rows*g.Cols; rc++ {
		from := g.Position{rc / g.Cols, rc % g.Cols}
		p := b.GetPiece(from)
		if p.Kind == g.Blank {
			continue
		}
		for rc_t := 0; rc_t < 9; rc_t++ {
			dr, dc := (rc_t/3)-1, (rc_t%3)-1
			to := g.Position{from.Row + dr, from.Col + dc}
			if b.CanMove(p.Owner.Turn(), from, to) {
				score += ControlPoint * colorCoef[p.Owner]
			}
		}
	}
	return score
}

// 評価値, 詰み手数, 手
func (p ConcAI) alphaBeta(b *g.Board, turn g.Turn, d, alpha, beta int, startAt time.Time, rec chan RecValue) {
	if time.Since(startAt).Seconds() > float64(p.ThinkSec) {
		rec <- RecValue{-Inf, d, nil, false}
		return
	}
	if b.IsGameEnd(turn ^ 1) {
		rec <- RecValue{-Inf, d, nil, true}
		return
	}
	if d < 0 {
		rec <- RecValue{eval(b) * colorCoef[turn.Color()], Inf, nil, true}
		return
	}
	var mvs = make([]g.Move, 1)
	mr := Inf
	movables := b.Movables(turn, false)
	recs := make([]chan RecValue, len(movables))
	for i, move := range movables {
		recs[i] = make(chan RecValue, 1)
		nb := g.DoMove(*b, move, turn)
		if time.Now().Nanosecond()%4 == 0 {
			go p.alphaBeta(&nb, turn^1, d-1, -beta, -alpha, startAt, recs[i])
		} else {
			p.alphaBeta(&nb, turn^1, d-1, -beta, -alpha, startAt, recs[i])
		}
	}
	for i, c := range recs {
		v := <-c
		if !v.success {
			rec <- RecValue{-Inf, d, nil, false}
			return
		}
		v.score *= -1
		if v.score > alpha || v.score == alpha && v.r < mr {
			alpha = v.score
			mr = v.r
			mvs[0] = movables[i]
		}
		if alpha >= beta {
			rec <- RecValue{alpha, mr, mvs, true}
			return
		}
	}
	rec <- RecValue{alpha, mr, mvs, true}
	return
}

func (p ConcAI) iterativeDeepening(b *g.Board, turn g.Turn) (int, g.Move) {
	t := time.Now()
	var d int
	var move g.Move
	for d = 0; ; d++ {
		ch := make(chan RecValue)
		go p.alphaBeta(b, p.Color.Turn(), d, -Inf, Inf, t, ch)
		recValue := <-ch
		close(ch)
		if recValue.success {
			move = recValue.moves[0]
		}
		if time.Since(t).Seconds() > float64(p.ThinkSec) {
			break
		}
	}
	return d, move
}

func (p ConcAI) NextMove(b *g.Board) g.Move {
	// s, _, m := p.alphaBeta(b, p.Color.Turn(), 0, -Inf, Inf)
	// fmt.Printf("%s score: %d", p.Color, s)
	d, move := p.iterativeDeepening(b, p.Color.Turn())
	fmt.Printf("%s ", p.Color)
	fmt.Printf("d: %d", d)
	fmt.Println()
	return move
}
