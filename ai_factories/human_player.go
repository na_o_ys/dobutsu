package ai_factories

import (
	g "../game"
	p "../human_player"
)

type HumanPFactory struct{}

func (f HumanPFactory) Create(c g.Color) g.Player {
	return p.HumanPlayer{c}
}

func (f HumanPFactory) Name() string {
	return "人間"
}
