package ai_factories

import (
	p "../conc_ai"
	g "../game"
	"fmt"
)

type ConcAIFactory struct {
	thinkSec int
}

func (f ConcAIFactory) Create(c g.Color) g.Player {
	return p.ConcAI{c, f.thinkSec}
}

func (f ConcAIFactory) Name() string {
	return fmt.Sprintf("並列 AI")
}
