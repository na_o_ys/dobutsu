package ai_factories

import (
	g "../game"
)

func Factories() []g.PlayerFactory {
	return []g.PlayerFactory{
		HumanPFactory{},
		RandomPFactory{},
		GasshukuAIFactory{5},
		GasshukuAISubFactory{4},
		ConcAIFactory{4},
		NoConcAIFactory{4},
		DConcAIFactory{4},
	}
}
