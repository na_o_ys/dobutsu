package ai_factories

import (
	g "../game"
	p "../gasshuku_ai"
	"fmt"
)

type GasshukuAIFactory struct {
	depth int
}

func (f GasshukuAIFactory) Create(c g.Color) g.Player {
	return p.GasshukuAI{c, f.depth}
}

func (f GasshukuAIFactory) Name() string {
	return fmt.Sprintf("合宿 AI(読み深さ %d)", f.depth)
}
