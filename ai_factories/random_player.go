package ai_factories

import (
	g "../game"
	p "../random_player"
	"math/rand"
	"time"
)

type RandomPFactory struct{}

func (f RandomPFactory) Create(c g.Color) g.Player {
	rand.Seed(time.Now().UnixNano())
	return p.RandomPlayer{c}
}

func (f RandomPFactory) Name() string {
	return "ランダム"
}
