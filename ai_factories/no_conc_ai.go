package ai_factories

import (
	g "../game"
	p "../no_conc_ai"
	"fmt"
)

type NoConcAIFactory struct {
	thinkSec int
}

func (f NoConcAIFactory) Create(c g.Color) g.Player {
	return p.NoConcAI{c, f.thinkSec}
}

func (f NoConcAIFactory) Name() string {
	return fmt.Sprintf("並列ナシ AI")
}
