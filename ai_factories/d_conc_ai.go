package ai_factories

import (
	p "../d_conc_ai"
	g "../game"
	"fmt"
)

type DConcAIFactory struct {
	thinkSec int
}

func (f DConcAIFactory) Create(c g.Color) g.Player {
	return p.DConcAI{c, f.thinkSec}
}

func (f DConcAIFactory) Name() string {
	return fmt.Sprintf("深さ並列 AI")
}
