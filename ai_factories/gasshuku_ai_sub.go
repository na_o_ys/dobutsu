package ai_factories

import (
	g "../game"
	p "../gasshuku_ai_sub"
	"fmt"
)

type GasshukuAISubFactory struct {
	depth int
}

func (f GasshukuAISubFactory) Create(c g.Color) g.Player {
	return p.GasshukuAISub{c, f.depth}
}

func (f GasshukuAISubFactory) Name() string {
	return fmt.Sprintf("合宿 AI Sub(読み深さ %d)", f.depth)
}
