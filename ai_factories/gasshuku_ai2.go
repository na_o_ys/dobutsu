package ai_factories

import (
	g "../game"
	p "../gasshuku_ai2"
	"fmt"
)

type GasshukuAI2Factory struct {
	depth int
}

func (f GasshukuAI2Factory) Create(c g.Color) g.Player {
	return p.GasshukuAI2{c, f.depth}
}

func (f GasshukuAI2Factory) Name() string {
	return fmt.Sprintf("合宿 AI 2(minimax, 読み深 %d)", f.depth)
}
