package human_player

import (
	g "../game"
	"fmt"
)

type HumanPlayer struct {
	Color g.Color
}

func (p HumanPlayer) NextMove(b *g.Board) g.Move {
	return ReadMove(b, p.Color.Turn())
}

func ReadMove(b *g.Board, turn g.Turn) g.Move {
	fmt.Println(turn.Color())
	movables := b.Movables(turn, false)
	for i, m := range movables {
		fmt.Printf("[%d] %s\n", i, m)
	}
	fmt.Print("input: ")
	var i int
	fmt.Scan(&i)
	return movables[i]
}
