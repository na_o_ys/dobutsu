package ai

import (
	g "../game"
	"math/rand"
)

type RandomPlayer struct {
	Color g.Color
}

func (p RandomPlayer) NextMove(b *g.Board) g.Move {
	ms := b.Movables(p.Color.Turn(), false)
	return ms[rand.Intn(len(ms))]
}
