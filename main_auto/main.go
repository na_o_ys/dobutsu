package main

import (
	f "../ai_factories"
	g "../game"
	"fmt"
	"runtime"
)

var factories = f.Factories()

func main() {
	cpus := runtime.NumCPU()
	fmt.Printf("cpus: %d", cpus)
	fmt.Println()
	runtime.GOMAXPROCS(cpus)
	var p [2]int
	var n int
	fmt.Printf("AI を選んでください:\n")
	for i, fa := range factories {
		fmt.Printf("[%d] %s\n", i, fa.Name())
	}
	fmt.Print("first AI: ")
	fmt.Scan(&p[0])
	fmt.Print("second AI: ")
	fmt.Scan(&p[1])
	fmt.Print("対戦数: ")
	fmt.Scan(&n)

	var res [3]int
	for i := 0; i < n; i++ {
		game := g.NewGame(
			factories[p[i%2]].Create(g.Black),
			factories[p[(i+1)%2]].Create(g.White),
			func(_ g.Turn, _ *g.Board, _ g.Move) {},
			func(c g.Color) {
				if c == g.Empty {
					fmt.Println("Draw")
					res[2]++
				} else {
					wf := factories[p[int(c)^(i%2)]]
					fmt.Printf("%s Win (%d/%d)\n", wf.Name(), i+1, n)
					res[int(c)^(i%2)]++
				}
			},
		)
		game.Run()
	}

	for i := 0; i < 2; i++ {
		fmt.Printf("%s: %d 勝\n", factories[p[i]].Name(), res[i])
	}
	if res[2] > 0 {
		fmt.Printf("%d 分け\n", res[2])
	}
}
