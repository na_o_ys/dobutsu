package gasshuku_ai2

import (
	g "../game"
	"fmt"
	"math"
)

type GasshukuAI2 struct {
	Color    g.Color
	MaxDepth int
}

func (p GasshukuAI2) Turn() g.Turn {
	return p.Color.Turn()
}

var pieceScore = map[g.PieceKind]int{
	g.Lion:     0,
	g.Hiyoko:   200,
	g.Kirin:    400,
	g.Zo:       500,
	g.Niwatori: 800,
}

var colorCoef = map[g.Color]int{g.Black: 1, g.White: -1}

const inHand = 0.5

func eval(b *g.Board) int {
	score := 0
	for rc := 0; rc < g.Rows*g.Cols; rc++ {
		p := b.GetPiece(g.Position{rc / g.Cols, rc % g.Cols})
		if p.Kind == g.Blank {
			continue
		}
		score += pieceScore[p.Kind] * colorCoef[p.Owner]
	}
	for i := 0; i < 2; i++ {
		for j, n := range b.Hands[i] {
			score += int(inHand * float64(pieceScore[g.PieceKind(j)]*n*colorCoef[g.Color(i)]))
		}
	}
	return score
}

var DummyMove = g.Move{g.Position{-1, -1}, g.Position{-1, -1}, g.Piece{g.Blank, g.Empty}}

func (p GasshukuAI2) dfs(b *g.Board, turn g.Turn, d int) (g.Move, int) {
	var mM g.Move
	mS := math.MinInt32
	if b.IsGameEnd(turn ^ 1) {
		return DummyMove, math.MinInt32
	}
	for _, move := range b.Movables(turn, false) {
		nb := g.DoMove(*b, move, turn)
		var s int
		if d >= p.MaxDepth {
			s = eval(b)
			s *= colorCoef[turn.Color()]
		} else {
			var m g.Move
			m, s = p.dfs(&nb, turn^1, d+1)
			if m == DummyMove {
				continue
			}
			s *= -1
		}
		if s > mS {
			mS = s
			mM = move
		}
	}
	return mM, mS
}

func (p GasshukuAI2) NextMove(b *g.Board) g.Move {
	move, score := p.dfs(b, p.Turn(), 0)
	fmt.Printf("score %d\n", score)
	return move
}
